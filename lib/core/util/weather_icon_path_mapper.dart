class WeatherIconPathMapper {
  static const Map<String, String> weatherCodePathMap = {
    '01d': 'assets/Sonne.png',
    '01n': 'assets/Sonne.png',
    '02d': 'assets/Sonne_Bewoelkt.png',
    '02n': 'assets/Sonne_Bewoelkt.png',
    '03d': 'assets/Bewoelkt.png',
    '03n': 'assets/Bewoelkt.png',
    '04d': 'assets/Bewoelkt_Dunkel.png',
    '04n': 'assets/Bewoelkt_Dunkel.png',
    '09d': 'assets/Regen.png',
    '09n': 'assets/Regen.png',
    '10d': 'assets/Sonne_Wolke_Regen.png',
    '10n': 'assets/Sonne_Wolke_Regen.png',
    '11d': 'assets/Gewitter.png',
    '11n': 'assets/Gewitter.png',
    '13d': 'assets/Schnee.png',
    '13n': 'assets/Schnee.png',
    '50d': 'assets/Nebel.png',
    '50n': 'assets/Nebel.png',
  };
}

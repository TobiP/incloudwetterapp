class WebException implements Exception {
  WebException(this.statusCode, this.message);
  final int statusCode;
  final String message;
}

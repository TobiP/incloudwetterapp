enum WeatherCondition {
  Sunny,
  Thunderstorm,
  Drizzle,
  Rain,
  Snow,
  Atmosphere,
  Clouds,
}

class HourlyTemperature {
  int hour;
  double temperature;
}

class Weather {
  Weather({
    this.location,
    this.weekday,
    this.weatherCondition,
    this.weatherIconCode,
    this.momentaryTemperature,
    this.maxTemperature,
    this.minTemperature,
    this.humidity,
    this.hourlyTemperatures,
  });

  final String location;
  String weekday;
  final WeatherCondition weatherCondition;
  final String weatherIconCode;
  final double momentaryTemperature;
  final double maxTemperature;
  final double minTemperature;
  final int humidity;

  final List<HourlyTemperature> hourlyTemperatures;
}

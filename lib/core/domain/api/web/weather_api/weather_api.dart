import 'package:incloudwetterapp/core/domain/api/web/http_client.dart';
import 'package:incloudwetterapp/core/domain/api/web/weather_api/weather_decoder.dart';
import 'package:incloudwetterapp/core/domain/entities/weather.dart';

class WeatherApi {
  WeatherApi(this.httpClient);

  final HttpClient httpClient;

  String baseUrl = 'https://api.openweathermap.org/data/2.5/weather?q=';
  String apiKeySuffix = '&APPID=61eff7720e51c7b5dc4e00d9433b158c';
  String celsiusSuffix = '&units=metric';

  String baseUrlDaily = 'https://api.openweathermap.org/data/2.5/forecast?q=';
  String dayCountSuffix = '&cnt=2';

  Future<Weather> loadWeather(String location) async {
    String url = baseUrl + location + apiKeySuffix + celsiusSuffix;
    print('URL: $url');
    String responseBody = await httpClient.get(url);
    Weather weather = WeatherDecoder.decode(responseBody);

    print(weather);
    return weather;
  }

  Future<Weather> loadTomorrowsWeather(String location) async {
    String url =
        baseUrlDaily + location + apiKeySuffix + celsiusSuffix + dayCountSuffix;

    print('URL: $url');
    String responseBody = await httpClient.get(url);

    return WeatherDecoder.decodeDays(responseBody);
  }
}

import 'dart:convert';

import 'package:incloudwetterapp/core/domain/entities/weather.dart';
import 'package:intl/intl.dart';

class WeatherDecoder {
  static Weather decode(String body) {
    print('response: $body');
    Weather weather = fromMap(json.decode(body));
    print(weather.humidity);
    print(weather.location);
    return weather;
  }

  static Weather decodeDays(String body) {
    print('response: $body');

    Weather weather = dailyFromMap(json.decode(body));
    print(weather.humidity);
    print(weather.location);
    return weather;
  }

  static Weather fromMap(Map<String, dynamic> map) {
    if (null == map) {
      return null;
    }
    return Weather(
      humidity: map['main']['humidity'],
      momentaryTemperature: map['main']['temp'],
      maxTemperature: _castToDouble(map['main']['temp_max']),
      minTemperature: _castToDouble(map['main']['temp_min']),
      location: map['name'],
      weatherIconCode: map['weather'][0]['icon'],
      weatherCondition: getWeatherConditionFromCode(map['weather'][0]['id']),
    );
  }

  static double _castToDouble(dynamic value) {
    if (value is double) {
      return value;
    }
    if (value is int) {
      return double.parse(value.toString());
    }
    throw FormatException('Could not cast String-value to double');
  }

  static Weather dailyFromMap(Map<String, dynamic> map) {
    if (null == map) {
      return null;
    }
    var dayTwo = map['list'][0];
    return Weather(
      humidity: dayTwo['main']['humidity'],
      momentaryTemperature: dayTwo['main']['temp'],
      maxTemperature: dayTwo['main']['temp_max'],
      minTemperature: dayTwo['main']['temp_min'],
      location: map['city']['name'],
      weatherIconCode: dayTwo['weather'][0]['icon'],
      weatherCondition: getWeatherConditionFromCode(dayTwo['weather'][0]['id']),
    );
  }

  static DateFormat weekDayFormatter = DateFormat('EEEE');

  static String getWeekDayFromUTC(int utcTime) =>
      weekDayFormatter.format(DateTime.utc(utcTime));

  static getWeatherConditionFromCode(int weatherCode) {
    if (weatherCode < 300) {
      return WeatherCondition.Thunderstorm;
    }
    if (weatherCode < 400) {
      return WeatherCondition.Drizzle;
    }
    if (weatherCode < 600) {
      return WeatherCondition.Rain;
    }
    if (weatherCode < 700) {
      return WeatherCondition.Atmosphere;
    }
    if (weatherCode < 800) {
      return WeatherCondition.Atmosphere;
    }
    if (weatherCode < 900) {
      return WeatherCondition.Clouds;
    }
  }
}

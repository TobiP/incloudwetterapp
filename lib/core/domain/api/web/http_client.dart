import 'dart:async';

import 'package:http/http.dart' as Http;
import 'package:incloudwetterapp/core/domain/exceptions/web_exception.dart';

class HttpClient {
  static const POST_HEADER = {"Content-Type": "application/json"};

  Future<String> get<T>(String url) async {
    final response = await Http.get(url);
    return _evaluateResponse(response);
  }

  Future<String> post<T>(String url, String body) async {
    final response = await Http.post(
      url,
      body: body,
      headers: POST_HEADER,
    );
    return _evaluateResponse(response);
  }

  Future<String> _evaluateResponse(Http.Response response) async {
    if (_responseIsValid(response)) {
      return response.body;
    }
    print(response.statusCode);
    print(response.reasonPhrase);
    throw WebException(response.statusCode, response.reasonPhrase);
  }

  bool _responseIsValid(Http.Response response) => response.statusCode == 200;
}

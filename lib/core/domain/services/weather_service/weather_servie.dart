import 'package:incloudwetterapp/core/domain/api/web/weather_api/weather_api.dart';
import 'package:incloudwetterapp/core/domain/api/web/weather_api/weather_decoder.dart';
import 'package:incloudwetterapp/core/domain/entities/weather.dart';
import 'package:incloudwetterapp/core/domain/services/weather_service/weather_state.dart';

class WeatherService {
  WeatherService(
    this._state,
    this._weatherApi,
  );
  final WeatherState _state;
  final WeatherApi _weatherApi;

  final String location = 'Darmstadt';

  WeatherState get state => _state;

  loadWeatherFromWeb() async {
    Weather todaysWeather = await _weatherApi.loadWeather(location);
    var now = DateTime.now();
    var tomorrow = now.add(Duration(days: 1));
    todaysWeather.weekday =
        WeatherDecoder.getWeekDayFromUTC(now.millisecondsSinceEpoch);
    _state.today = todaysWeather;

    Weather tomorrowsWeather = await _weatherApi.loadTomorrowsWeather(location);
    tomorrowsWeather.weekday =
        WeatherDecoder.getWeekDayFromUTC(tomorrow.millisecondsSinceEpoch);
    _state.tomorrow = tomorrowsWeather;
  }
}

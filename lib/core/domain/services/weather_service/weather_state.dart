import 'package:flutter/material.dart';
import 'package:incloudwetterapp/core/domain/entities/weather.dart';

class WeatherState extends ChangeNotifier {
  Weather _today;
  Weather _tomorrow;

  Weather get today => _today;
  Weather get tomorrow => _tomorrow;

  set today(Weather weather) {
    _today = weather;
    print(_today);
    notifyListeners();
  }

  set tomorrow(Weather weather) {
    _tomorrow = weather;
    notifyListeners();
  }
}

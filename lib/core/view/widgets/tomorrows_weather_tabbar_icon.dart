import 'package:flutter/material.dart';
import 'package:incloudwetterapp/core/view/widgets/generic_tab_bar/tab_bar_item.dart';

class TomorrowsWeatherTabbarIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) => TabBarItem(Column(
        children: <Widget>[
          _createIcon(context),
          Text('Morgen'),
        ],
      ));

  _createIcon(BuildContext context) => Image.asset(
        'assets/Icon/morgen.png',
        height: 24, //TODO: find default size definition
        color: IconTheme.of(context).color,
      );
}

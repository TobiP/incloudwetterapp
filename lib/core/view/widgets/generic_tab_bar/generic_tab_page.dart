import 'package:flutter/material.dart';

import 'package:incloudwetterapp/core/view/widgets/generic_tab_bar/tab_bar_item.dart';

class GenericTabPage extends StatelessWidget {
  const GenericTabPage({
    this.tabItems,
    this.tabs,
    this.appBarTitle,
  });

  final List<Widget> tabItems;
  final List<Widget> tabs;
  final Widget appBarTitle;

  @override
  Widget build(BuildContext context) => DefaultTabController(
        length: tabItems.length,
        child: Scaffold(
          appBar: AppBar(title: appBarTitle),
          body: TabBarView(children: tabs),
          bottomNavigationBar: TabBar(
            tabs: tabItems,
            labelColor: Colors.amber,
            indicatorColor: Colors.amber,
            unselectedLabelColor: Theme.of(context).unselectedWidgetColor
          ),
        ),
      );
}

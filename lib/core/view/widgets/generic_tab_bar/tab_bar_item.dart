import 'package:flutter/material.dart';

class TabBarItem extends StatelessWidget {
  const TabBarItem(this.child);

  final Widget child;

  @override
  Widget build(BuildContext context) => Container(
        height: kToolbarHeight / 1.2,
        child: Center(child: child),
      );
}

import 'package:flutter/material.dart';
import 'package:incloudwetterapp/core/view/widgets/generic_tab_bar/tab_bar_item.dart';

class TodaysWeatherTabbarIcon extends TabBarItem {
  TodaysWeatherTabbarIcon()
      : super(Column(
          children: <Widget>[
            Icon(Icons.wb_sunny),
            Text('Heute'),
          ],
        ));
}

import 'package:flutter/material.dart';

import 'package:incloudwetterapp/core/view/widgets/generic_tab_bar/generic_tab_page.dart';
import 'package:incloudwetterapp/core/view/widgets/todays_weather_tabbar_icon.dart';
import 'package:incloudwetterapp/core/view/widgets/tomorrows_weather_tabbar_icon.dart';
import 'package:incloudwetterapp/features/days_weather/view/widgets/days_weather/days_wather.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => GenericTabPage(
        tabItems: [
          TodaysWeatherTabbarIcon(),
          TomorrowsWeatherTabbarIcon(),
        ],
        tabs: [
          DaysWeather(dayToDisplay: DayToDisplay.TODAY),
          DaysWeather(dayToDisplay: DayToDisplay.TOMORROW),
        ],
      );
}

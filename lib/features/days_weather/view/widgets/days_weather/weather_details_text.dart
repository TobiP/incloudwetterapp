import 'package:flutter/material.dart';

import 'package:incloudwetterapp/core/domain/entities/weather.dart';

class WeatherDetailsText extends StatelessWidget {
  const WeatherDetailsText({this.weather});
  final Weather weather;

  @override
  Widget build(BuildContext context) => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('DETAILS'),
          SizedBox(height: 5),
          _createWeatherStateText(),
          _createMaxTemperatureText(),
          _createHumidityText(),
        ],
      );

  Widget _createWeatherStateText() => Text('Heute: ${weather.weatherCondition}');

  Widget _createMaxTemperatureText() =>
      Text('Max Temperature: ${weather.maxTemperature}');

  Widget _createHumidityText() => Text('Humidity ${weather.humidity}');
}

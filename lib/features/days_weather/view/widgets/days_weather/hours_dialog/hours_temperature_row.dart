import 'package:flutter/material.dart';

class HoursTemperatureRow extends StatelessWidget {
  final String dayTime;
  final double temperature;

  const HoursTemperatureRow({this.dayTime, this.temperature});
  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        
        children: <Widget>[
          Text(dayTime),
          SizedBox(width: 10),
          Text('$temperature°C'),
        ],
      );
}

import 'package:flutter/material.dart';
import 'package:incloudwetterapp/core/domain/entities/weather.dart';
import 'package:incloudwetterapp/features/days_weather/view/widgets/days_weather/hours_dialog/hours_temperature_row.dart';

class HoursDialog extends StatelessWidget {
  const HoursDialog({this.weather});
  final Weather weather;

  @override
  Widget build(BuildContext context) => AlertDialog(
        title: Text('Temperaturen des Tages'),
        content: _createContent(),
        actions: _createActions(context),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      );

  List<Widget> _createActions(BuildContext context) => <Widget>[
        FlatButton(
          child: Text('OK'),
          onPressed: () => Navigator.pop(context),
        )
      ];

  Column _createContent() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        HoursTemperatureRow(dayTime: '10:00', temperature: 5),
        HoursTemperatureRow(dayTime: '12:00', temperature: 6),
        HoursTemperatureRow(dayTime: '14:00', temperature: 7),
        HoursTemperatureRow(dayTime: '16:00', temperature: 8),
      ],
    );
  }
}

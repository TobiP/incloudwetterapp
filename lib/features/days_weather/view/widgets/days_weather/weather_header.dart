import 'package:flutter/material.dart';

import 'package:incloudwetterapp/core/domain/entities/weather.dart';

class WeatherHeader extends StatelessWidget {
  const WeatherHeader({this.weather});
  final Weather weather;

  @override
  Widget build(BuildContext context) {
    print('WeatherHeader: ${weather.location}');
    return Column(
      children: <Widget>[
        _createLocationText(),
        _createWeekdayText(),
      ],
    );
  }

  Widget _createLocationText() => Text(weather?.location, textScaleFactor: 2);

  Widget _createWeekdayText() => Text(weather?.weekday);
}

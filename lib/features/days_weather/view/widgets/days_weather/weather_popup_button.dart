import 'package:flutter/material.dart';

import 'package:incloudwetterapp/core/domain/entities/weather.dart';
import 'package:incloudwetterapp/features/days_weather/view/widgets/days_weather/hours_dialog/hours_dialog.dart';

class WeatherPopupButton extends StatelessWidget {
  WeatherPopupButton({this.weather});
  final Weather weather;

  Size _screenSize;

  double get _cloudSize => _screenSize.height / 16;
  double get _dotSize => _screenSize.height / 25;

  double get _dotsPositionBottom => _screenSize.height / 250;
  double get _dotsPositionLeft => _screenSize.height / 85;

  void _openHoursDialog(BuildContext context) => showDialog(
        context: context,
        builder: (_) => HoursDialog(),
      );

  Color _getDotsColor(BuildContext context) =>
      Theme.of(context).scaffoldBackgroundColor;

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () => _openHoursDialog(context),
      child: _createIcon(context),
    );
  }

  Widget _createIcon(BuildContext context) => Stack(
        children: <Widget>[
          _createCloudIcon(context),
          _createDotsIcon(context),
        ],
      );

  Widget _createCloudIcon(BuildContext context) => Icon(
        Icons.cloud,
        size: _cloudSize,
        color: Colors.amber,
      );

  Widget _createDotsIcon(BuildContext context) => Positioned(
        bottom: _dotsPositionBottom,
        left: _dotsPositionLeft,
        child: Icon(
          Icons.more_horiz,
          size: _dotSize,
          color: _getDotsColor(context),
        ),
      );
}

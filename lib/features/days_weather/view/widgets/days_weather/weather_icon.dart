import 'dart:math';

import 'package:flutter/material.dart';

import 'package:incloudwetterapp/core/domain/entities/weather.dart';
import 'package:incloudwetterapp/core/util/weather_icon_path_mapper.dart';

class WeatherIcon extends StatelessWidget {
  const WeatherIcon({this.weather});
  final Weather weather;

  @override
  Widget build(BuildContext context) => Image.asset(
        _getIconAssetPath(),
        height: _getSize(context),
      );

  double _getSize(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return min(screenSize.width, screenSize.height) / 3;
  }

  String _getIconAssetPath() =>
      WeatherIconPathMapper.weatherCodePathMap[weather.weatherIconCode];
}

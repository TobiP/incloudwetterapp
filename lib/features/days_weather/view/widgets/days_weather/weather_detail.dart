import 'package:flutter/material.dart';

import 'package:incloudwetterapp/core/domain/entities/weather.dart';
import 'package:incloudwetterapp/features/days_weather/view/widgets/days_weather/weather_details_text.dart';

class WeatherDetail extends StatelessWidget {
  const WeatherDetail({Key key, this.weather}) : super(key: key);
  final Weather weather;

  @override
  Widget build(BuildContext context) => Row(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _createTemperatureText(),
          SizedBox(width: 10),
          WeatherDetailsText(weather: weather),
        ],
      );

  _createTemperatureText() => Text('${weather.momentaryTemperature} Grad C');
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:incloudwetterapp/core/domain/entities/weather.dart';
import 'package:incloudwetterapp/core/domain/services/weather_service/weather_state.dart';
import 'package:incloudwetterapp/features/days_weather/view/widgets/days_weather/weather_detail.dart';
import 'package:incloudwetterapp/features/days_weather/view/widgets/days_weather/weather_icon.dart';
import 'package:incloudwetterapp/features/days_weather/view/widgets/days_weather/weather_popup_button.dart';
import 'package:incloudwetterapp/features/days_weather/view/widgets/days_weather/weather_header.dart';

enum DayToDisplay { TODAY, TOMORROW }

class DaysWeather extends StatelessWidget {
  const DaysWeather({this.dayToDisplay});

  final DayToDisplay dayToDisplay;

  @override
  Widget build(BuildContext context) =>
      Consumer<WeatherState>(builder: _builder);

  Widget _builder(BuildContext context, WeatherState state, Widget child) {
    Weather weather = _getSelectedWeather(state);
    print('DaysWeather location: ${weather.location}');
    print('Weather code: ${weather.weatherIconCode}');
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        WeatherHeader(weather: weather),
        WeatherIcon(weather: weather),
        WeatherDetail(weather: weather),
        WeatherPopupButton(weather: weather),
      ],
    );
  }

  Weather _getSelectedWeather(WeatherState state) =>
      dayToDisplay == DayToDisplay.TODAY ? state.today : state.tomorrow;
}

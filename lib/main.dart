import 'package:flutter/material.dart';
import 'package:incloudwetterapp/core/domain/services/weather_service/weather_servie.dart';

import 'package:incloudwetterapp/core/view/pages/main_page.dart';
import 'package:incloudwetterapp/di/di.dart';
import 'package:incloudwetterapp/di/locator.dart';
import 'package:incloudwetterapp/di/weather_state_provider.dart';

void main() {
  DependencyInjector.buildApp();
  locator<WeatherService>().loadWeatherFromWeb();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => WeatherStateProvider(
        MaterialApp(
          darkTheme: ThemeData.dark(),
          routes: {
            '/': (BuildContext context) => MainPage(),
          },
        ),
      );
}

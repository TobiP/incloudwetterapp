import 'package:flutter/material.dart';
import 'package:incloudwetterapp/core/domain/services/weather_service/weather_state.dart';
import 'package:incloudwetterapp/di/locator.dart';
import 'package:provider/provider.dart';

class WeatherStateProvider extends MultiProvider {
  WeatherStateProvider(Widget child)
      : super(
          providers: [
            ChangeNotifierProvider.value(value: locator<WeatherState>()),
          ],
          child: child,
        );
}

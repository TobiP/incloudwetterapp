import 'package:incloudwetterapp/core/domain/api/web/http_client.dart';
import 'package:incloudwetterapp/core/domain/api/web/weather_api/weather_api.dart';
import 'package:incloudwetterapp/core/domain/services/weather_service/weather_servie.dart';
import 'package:incloudwetterapp/core/domain/services/weather_service/weather_state.dart';
import 'package:incloudwetterapp/di/fake.dart';
import 'package:incloudwetterapp/di/locator.dart';

class DependencyInjector {
  static void buildApp() {
    registerHttpClient();
    registerWeatherApi();
    registerWeatherState();
    registerWeatherService();
    Fake.fakeTheWeather();
  }

  static void registerHttpClient() {
    locator.registerSingleton(HttpClient());
  }

  static void registerWeatherApi() {
    locator.registerSingleton(WeatherApi(locator<HttpClient>()));
  }

  static void registerWeatherState() {
    locator.registerSingleton(WeatherState());
  }

  static void registerWeatherService() {
    locator.registerSingleton(WeatherService(
      locator<WeatherState>(),
      locator<WeatherApi>(),
    ));
  }
}

import 'package:incloudwetterapp/core/domain/entities/weather.dart';
import 'package:incloudwetterapp/core/domain/services/weather_service/weather_state.dart';
import 'package:incloudwetterapp/di/locator.dart';

class Fake {
  static fakeTheWeather() {
    var weatherState = locator<WeatherState>();
    weatherState.today = Weather(
      location: 'Darmstadt',
      weekday: 'Montag',
      weatherCondition: WeatherCondition.Clouds,
      momentaryTemperature: 20,
      maxTemperature: 26,
      humidity: 30,
      weatherIconCode: '01n',

    );
    weatherState.tomorrow = Weather(
      location: 'Darmstadt',
      weekday: 'Dienstag',
      weatherCondition: WeatherCondition.Sunny,
      momentaryTemperature: 21,
      maxTemperature: 27,
      humidity: 31,
      weatherIconCode: '01n',
    );
  }
}
